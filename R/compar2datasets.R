#'Compare two different sagacity datasets having supposedly same accessions and same markers
#'
#'The function makes a comparison of two datasets based on accession and locus names.
#'
#'@param x A dataframe as obtained from dataset.fetch or dataset.fetch.WS with a least two datasets. Columns names are used, thus x must have the following headers: "DATASET  GERMPLASM_ID	LOCUS	ALLELE	AMOUNT	ACCESSION_ID", but order of columns is indifferent
#'@param sets An optional vector with the names of the two data datasets to compare (default to first two datasets of x)
#'@param locrename An optional dataframe to change locus names and make them compatible between datasets (two columns with original and new names)
#'@param locexclude An optional vector of locus names to be excluded from the analysis
#'@param accexclude An optional vector of accession names to be excluded from the analysis
#'@param counts if TRUE all counts are returned as absolute values instead of percentages
#'@details If the two datasets have different locus and/or accession names, user is warned and the intersect is used.The first analysis step is to look at number of alleles per datapoints. This is mainly a diploid seed crop rationale.
#'@return A list with the following components
#'@return -Nb of acc/locus with diff number of alleles
#'@return -Nb of locus/acc with diff number of alleles
#'@return -Soft comparison table
#'@return -Hard comparison table
#'@return -Soft comparison loc stats
#'@return -Hard comparison loc stats
#'@return -Soft comparison accession stats
#'@return -Hard comparison accession stats
#'@return -Locus list
#'@return -Accession List



#'@author J.-F. Rami \email{rami@@cirad.fr}
#'@examples
#'\dontrun{
#' compar2datasets(x)
#'}

compar2datasets <-
function(x,sets=unique(x$DATASET)[1:2],locrename = NULL,locexclude=NULL, accexclude=NULL, counts=F){
#
#Compare 2 different datasets provided in the x dataframe 
#formated as follow
#DATASET	GERMPLASM_ID	LOCUS	ALLELE	AMOUNT	ACCESSION_ID
#Proj1	08SS001	mSbCIR238	73	1	IS13
#Proj1	08SS001	mSbCIR240	112	1	IS13
#Proj1	08SS001	mSbCIR248	101	0.5	IS13
#Proj1	08SS001	mSbCIR248	95	0.5	IS13
#Proj1	08SS001	mSbCIR300	108	1	IS13
#Proj2	08SS001	mSbCIR238	73	1	IS13
#Proj2	08SS001	mSbCIR240	112	1	IS13
#Proj2	08SS001	mSbCIR248	101	0.5	IS13
#Proj2	08SS001	mSbCIR248	95	0.5	IS13
#Proj2	08SS001	mSbCIR300	108	1	IS13
#Proj2	08SS001	Xcup14	213	1	IS13
#...

#All columns are mandatory
#columns names are used, thus x must have these names as headers, but order of columns is indifferent

#if project1 and project2 have different marker names, user is warned and the intersect is used

# x can contain more than 2 datasets. The names of the 2 datasets to compare are given as argument to the function:
#usage : compar2projects(x,projects=c("Proj1","Proj2")){

if (!is.null(locrename)){
	 x$LOCUS[x$LOCUS%in%locrename[,1]]<-locrename[match(x$LOCUS[x$LOCUS%in%locrename[,1]],locrename[,1]),2]
	 x$LOCUS<-factor(x$LOCUS)
}
if (!is.null(locexclude)){
	x<-x[!x$LOCUS%in%locexclude,]
}
if (!is.null(accexclude)){
	x<-x[!x$ACCESSION_ID%in%accexclude,]
}

# uppercase headers
colnames(x)<-toupper(colnames(x))

#create subset of x corresponding to the 2 datasets
x<-subset(x,x$DATASET==sets[1] | x$DATASET==sets[2])
#update factors
x$DATASET<-factor(x$DATASET)

#check that Locus list is exactly the same for both projects
locset1<-unique(subset(x,x$DATASET==as.character(sets[1]))$LOCUS)
locset2<-unique(subset(x,x$DATASET==as.character(sets[2]))$LOCUS)
comlocs<-NULL
if (sum(!locset1 %in% locset2) > 0 || sum(!locset2 %in% locset1) > 0 ){
	print("non common set of Loci between both dataset. Using intersect")
	print("Dataset 1:")
	print(as.character(locset1[!locset1 %in% locset2]))
	print("Dataset 2:")
	print(as.character(locset2[!locset2 %in% locset1]))
	comlocs<-intersect(locset1,locset2)
	x<-subset(x,x$LOCUS %in% comlocs)
}
#check that accession list is exactly the same for both projects
accset1<-unique(subset(x,x$DATASET==as.character(sets[1]))$ACCESSION_ID)
accset2<-unique(subset(x,x$DATASET==as.character(sets[2]))$ACCESSION_ID)

comaccs<-NULL
if (sum(!accset1 %in% accset2) > 0 || sum(!accset2 %in% accset1) > 0 ){
	print("non common set of Accessions between both dataset. Using intersect")
	print("Dataset 1:")
	print(as.character(accset1[!accset1 %in% accset2]))
	print("Dataset 2:")
	print(as.character(accset2[!accset2 %in% accset1]))
	comaccs<-intersect(accset1,accset2)
	x<-subset(x,x$ACCESSION_ID %in% comaccs)
}

#compute stats in terms of number of alleles
x$LOCUS<-factor(x$LOCUS)
x$ACCESSION_ID<-factor(x$ACCESSION_ID)
x$ALLELE<-as.numeric(x$ALLELE)
x.agg<-aggregate(x$AMOUNT,by = list(DATASET=x$DATASET,LOCUS=x$LOCUS,ACCESSION_ID=x$ACCESSION_ID),length)
x.split<- split(x.agg,list(x.agg$LOCUS,x.agg$ACCESSION_ID))
x.differentnbofalleles<-lapply(x.split,function(a) if (nrow(a)>1) {if (!a$x[1]==a$x[2]){return(1)} else {return(0)}})
if (!sum(unlist(x.differentnbofalleles))==0){
	locuslist <- unlist(lapply(strsplit(names(unlist(x.differentnbofalleles)[unlist(x.differentnbofalleles)==1]),"\\."),function(a) a[1]))
	accessionlist <- unlist(lapply(strsplit(names(unlist(x.differentnbofalleles)[unlist(x.differentnbofalleles)==1]),"\\."),function(a) a[2]))
	locstats<- aggregate(locuslist,list(LOCUS=locuslist),length)
	accstats<- aggregate(accessionlist,list(ACCESSION_ID=accessionlist),length)
	names(locstats)<-c("Locus","Counts")
	names(accstats)<-c("Accession_ID","Counts")
	}
else {
	locstats<- character(0)
	accstats<- character(0)
	}

#browser()
#dataset comparison
loc<-unique(x$LOCUS)
loc<-sort(loc)
ind<-unique(x$ACCESSION_ID)
ind<-sort(ind)
x.splped<-split(x,list(x$LOCUS,x$ACCESSION_ID))
x.splped.comp1<-lapply(x.splped,function(a) genotcompar(a,F))
x.splped.comp1.unlist<-unlist(x.splped.comp1)
x.splped.comp1.unlist.tab<- matrix(x.splped.comp1.unlist,nrow=length(ind),ncol=length(loc),byrow=T)
x.splped.comp2<-lapply(x.splped,function(a) genotcompar(a,T))
x.splped.comp2.unlist<-unlist(x.splped.comp2)
x.splped.comp2.unlist.tab<- matrix(x.splped.comp2.unlist,nrow=length(ind),ncol=length(loc),byrow=T)
x.splped.comp3<-lapply(x.splped,function(a) genotcompar2(a))
x.splped.comp3.unlist<-unlist(x.splped.comp3)
x.splped.comp3.unlist.tab<- matrix(x.splped.comp3.unlist,nrow=length(ind),ncol=length(loc),byrow=T)
colnames(x.splped.comp1.unlist.tab)<-loc
rownames(x.splped.comp1.unlist.tab)<-ind
colnames(x.splped.comp2.unlist.tab)<-loc
rownames(x.splped.comp2.unlist.tab)<-ind
colnames(x.splped.comp3.unlist.tab)<-loc
rownames(x.splped.comp3.unlist.tab)<-ind


#format output as a list
result<-vector("list",11)
names(result)<-c("Nb of acc/locus with diff number of alleles","Nb of locus/acc with diff number of alleles","Soft comparison table","Hard comparison table","Soft comparison loc stats","Hard comparison loc stats","Soft comparison accession stats","Hard comparison accession stats","Locus list","Accession List")
result[[1]]<-locstats
result[[2]]<-accstats
result[[3]]<-x.splped.comp1.unlist.tab
result[[4]]<-x.splped.comp2.unlist.tab
if (counts) {
	result[[5]]<-round(apply(result[[3]],2,function(a) sum(a,na.rm=T)),0)
	result[[6]]<-round(apply(result[[4]],2,function(a) sum(a,na.rm=T)),0)
	result[[7]]<-round(apply(result[[3]],1,function(a) sum(a,na.rm=T)),0)
	result[[8]]<-round(apply(result[[4]],1,function(a) sum(a,na.rm=T)),0)	
	}
else {
	result[[5]]<-round(apply(result[[3]],2,function(a) sum(a,na.rm=T))/apply(result[[3]],2,function(a) sum(!is.na(a)))*100,0)
	result[[6]]<-round(apply(result[[4]],2,function(a) sum(a,na.rm=T))/apply(result[[4]],2,function(a) sum(!is.na(a)))*100,0)
	result[[7]]<-round(apply(result[[3]],1,function(a) sum(a,na.rm=T))/apply(result[[3]],1,function(a) sum(!is.na(a)))*100,0)
	result[[8]]<-round(apply(result[[4]],1,function(a) sum(a,na.rm=T))/apply(result[[4]],1,function(a) sum(!is.na(a)))*100,0)
	}
if (is.null(comlocs)){
	result[[9]]<-locset1
	}else{
	result[[9]]<-comlocs
}
if (is.null(comaccs)){
	result[[10]]<-accset1
	}else{
	result[[10]]<-comaccs
}
result[[11]]<-x.splped.comp3.unlist.tab
return(result)
}

