#'Fetch a dataset from sagacity database
#'
#'dataset.fetch uses either RODBC or RmySQL to retrieve a dataset from sagacity database.
#'This version of dataset.fecth assumes that genotyping data by germplasm/accession summary is stored in the database. This should be deprecated in the future as new version of sagacity implements dynamic data summary available through a web-service. See dataset.fetch.WS function to use this feature.
#'For now the dataset.fetch only allows to fetch germplasm datasets
#'
#'@param mode specify whether to use RODBC or RmySQL depending on the sagacity database engine (MsAccess or MySQL respectively)
#'@param target specify whether the databse is accessible as a local database (mdbfile) or on a server
#'@param sets a character vector with the name(s) of dataset(s) to retrieve
#'@param choose boolean, if TRUE the function provide the list of available datasets and ask the user to choose
#'@param server server name or IP address in case target=server
#'@param UID username for database access
#'@param PWD password for database access
#'@param PORT port for database access
#'@param socket 
#'@param mdbfiledir path to mdbfile in case target=mdbfile
#'@param dbname either database name on server of mdbfile filename
#'@param driver DBI driver
#'@return A data.frame with the folowing columns: DATASET  LOCUS  GERMPLASM_ID ALLELE  AMOUNT ACCESSION_ID
#'@author J.-F. Rami \email{rami@@cirad.fr}
#'@examples
#'\dontrun{
#' x<-dataset.fetch(mode="RmySQL",target="server",choose=T,server="lomagne.cirad.fr",UID="Sagacity_User",PWD="43mju5p",dbname="Sagacity_Sorghum2")
#'}
dataset.fetch <-
function(mode=c("RODBC","RmySQL"),target=c("mdbfile","server"),sets=NA,choose=F,server=NULL,UID=NULL,PWD=NULL,PORT=3306,socket="/Applications/MAMP/tmp/mysql/mysql.sock",mdbfiledir="C:\\Sagacity\\data",dbname=NA,driver="{MySQL ODBC 5.1 Driver}"){

if(!is.null(server)){
	if(is.null(UID) | is.null(PORT) | is.null(socket)){
		stop("Please fill in all connection parameters")
		}
	target<-"server"}
else{
	target<-"mdbfile"}
	
		

if (mode=="RODBC"){
	if (target=="mdbfile"){
		dbcon<-paste('DRIVER=Microsoft Access Driver (*.mdb);UID=admin;UserCommitSync=Yes;Threads=3;SafeTransactions=0;PageTimeout=5;MaxScanRows=8;MaxBufferSize=2048;FIL=MS Access;DriverId=25;DefaultDir=',mdbfiledir,';DBQ=',paste(mdbfiledir,dbname,sep="\\"),sep="")
	}else{
		if (is.null(PWD)){
			PWD<-readline("password: ")
		}
		dbcon<-paste("DRIVER=",driver,";SERVER=",server,";DATABASE=",dbname,";UID=",UID,";PWD=",PWD,";PORT=",PORT,";OPTION=3",sep="")
	}

}else{
	if (target!="server"){
		return(paste("Wrong combination of parameters: mode=",mode,",target=",target,sep=""))
	}else{
		if (is.null(PWD)){
			PWD<-readline("password: ")
		}
		dbcon<-c(UID,PWD,server,PORT,socket,dbname)
	}
	
}


whereclause<- "WHERE "
if (choose) {
	if (mode=='RODBC') {
		sagacity<-odbcDriverConnect(dbcon)
		allsets<-sqlQuery(sagacity,"SELECT DATASET_ID FROM DAT_Dataset ORDER BY DATASET_ID",as.is=T)
		sets<-select.list(as.vector(allsets[,1]),multiple=T)
		odbcClose(sagacity)
	}
else{
		sagacity<-dbConnect(MySQL(),user=dbcon[1], password=dbcon[2], dbname=dbcon[6], host=dbcon[3], port = as.numeric(dbcon[4]), unix.socket=dbcon[5])
		allsets<-dbGetQuery(sagacity,"SELECT DATASET_ID FROM DAT_Dataset ORDER BY DATASET_ID")
		sets<-select.list(as.vector(allsets[,1]),multiple=T)
		mysqlCloseConnection(sagacity)
	}
}
for (set in sets) {
	whereclause<- paste(whereclause, "D.DATASET_ID='",set, "' OR ",sep="")
}
#delete the last "' OR" in whereclause
whereclause<-substr(whereclause, 1, nchar(whereclause) - 4)

if (mode=='RODBC') {
		sagacity<-odbcDriverConnect(dbcon)
		sqlstring<-paste("SELECT D.DATASET_ID, DL.LOCUS, DG.GERMPLASM_ID, GAR.ALLELE, GAR.ALLELE_AMount, G.Gp_ACCESSION FROM (((DAT_Dataset D INNER JOIN DAT_Dataset_Loc DL ON D.DATASET_ID = DL.DATASET_ID) INNER JOIN DAT_Dataset_Ger DG ON D.DATASET_ID = DG.DATASET_ID) INNER JOIN DAT_Germplasm_all_results GAR ON (GAR.LOCUS = DL.LOCUS) AND (DG.GERMPLASM_ID = GAR.GERMPLASM_ID)) INNER JOIN DAT_Germplasm G ON DG.GERMPLASM_ID = G.GERMPLASM_ID ",whereclause, sep="")
		sets<-sqlQuery(sagacity,sqlstring,as.is=T)
		odbcClose(sagacity)
		} 
else{
		sagacity<-dbConnect(MySQL(),user=dbcon[1], password=dbcon[2], dbname=dbcon[6], host=dbcon[3], port = as.numeric(dbcon[4]), unix.socket=dbcon[5])
		sqlstring<-paste("SELECT D.DATASET_ID, DL.LOCUS, DG.GERMPLASM_ID, GAR.ALLELE, GAR.ALLELE_AMount, G.Gp_ACCESSION FROM (((DAT_Dataset D INNER JOIN DAT_Dataset_Loc DL ON D.DATASET_ID = DL.DATASET_ID) INNER JOIN DAT_Dataset_Ger DG ON D.DATASET_ID = DG.DATASET_ID) INNER JOIN DAT_Germplasm_all_results GAR ON (GAR.LOCUS = DL.LOCUS) AND (DG.GERMPLASM_ID = GAR.GERMPLASM_ID)) INNER JOIN DAT_Germplasm G ON DG.GERMPLASM_ID = G.GERMPLASM_ID ",whereclause, sep="")
		sets<-dbGetQuery(sagacity,sqlstring)
		mysqlCloseConnection(sagacity)
		}
colnames(sets)<-c("DATASET","LOCUS","GERMPLASM_ID","ALLELE","AMOUNT","ACCESSION_ID")
sets
}

